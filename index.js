const { processInputFn, emptyBouquet } = require('./production.js');
const spawn = require('child_process').spawn;

const mainFn = (filename, state) => {
    try {
        var tail = spawn('tail', ['-f', '-n', '+1', filename]);
        tail.stdout.on('data', function (data) {
            data.toString().split(/\r?\n/).forEach((input) => processInputFn(input, state));
        });
    }
    catch (e) {
        console.log(e);
        process.exit(1);
    }
};

const initialState = {
    bouquetQueue: [],
    activeBouquet: emptyBouquet,
    storageLimit: false,
    storage: {
    },
    ready: []
};

const STORAGE_LIMIT_FLAG = '--storage-limit';
const args = process.argv.slice(2);
var [filename, flag] = args.sort().reverse(); // sort for filename to be first and --flag last
if (!filename || args.length > 2 || (flag && flag !== STORAGE_LIMIT_FLAG))
    return console.log(`Wrong number of arguments. Usage: node index.js inputFile \r\n Optional ${STORAGE_LIMIT_FLAG} flag`);
if (flag === STORAGE_LIMIT_FLAG)
    initialState.storageLimit = true;

mainFn((filename || './sample/input.txt'),
    initialState
);