# specify the node base image with your desired version node:<version>
FROM node:6
COPY package.json .
RUN npm install
COPY . /
CMD node index.js
EXPOSE 8081
