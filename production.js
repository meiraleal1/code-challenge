const emptyBouquet = {};
const processInputFn = (input, state) => {
    const activeBouquet = state.activeBouquet;
    if (!input)
        return;
    if (input.length === 2 && /^[a-z]*$/.test(input[0]) && /^[SL]*$/.test(input[1])) { // validate flowers entry
        if (state.storageLimit && isStorageFull(state))
            throw ("Storage is full.")
        if (activeBouquet && activeBouquet.size === input[1] && activeBouquet.flowers[input[0]]) {
            activeBouquet.flowers[input[0]] -= 1;
            activeBouquet.total -= 1;
        }
        state.storage[input[1] + input[0]] = (state.storage[input[1] + input[0]] || 0) + 1;
    }
    if (input.length > 2 && /^[A-Z]*$/.test(input[0]) && /^[A-Z]*$/.test(input[1])) { // validate bouquet entry
        const bouquet = parseBouquetSpec(input);
        if (activeBouquet === emptyBouquet)
            state.activeBouquet = bouquet;
        state.bouquetQueue.push(bouquet);
    }
    return updateState(state);
};

const parseBouquetSpec = (activeBouquet) => {
    const [input, name, size, spec, _, total] = activeBouquet.match(/^(\S)([L|S])((\d+\S)*)(\d+)$/);
    const flowers = spec.split(/(\d+)/).filter(spec => spec).reverse().reduce((curr, next, index, arr) => {
        if (isNaN(next)) {
            curr[next] = parseInt(arr[index + 1]);
        }
        return curr;
    }, {});
    return { input, name, size, total, flowers };
};


const isStorageFull = (state) => {
    const flowers = Object.keys(state.storage);
    const total = parseInt(flowers.reduce((curr, next) => curr + state.storage[next], 0));
    return total === 256
}

const updateState = (state) => {
    const bouquet = state.activeBouquet;
    if (bouquet === emptyBouquet)
        return state;
    const spec_flowers = Object.keys(bouquet.flowers).reduce((curr, next) => curr + bouquet.flowers[next], 0);
    //console.log(spec_flowers)
    if (bouquet.total === 0) {
        console.log(bouquet.input);
        state.ready.push(bouquet);
        state.bouquetQueue = state.bouquetQueue.slice(1);
        state.activeBouquet = state.bouquetQueue[0] || emptyBouquet;
    }
    else {
        if (spec_flowers > 0) {
            //return updateState(state);
        }
        else {
            const store = Object.keys(state.storage).filter(flowerType => flowerType[0] === bouquet.size).sort((a, b) => state.storage[b] - state.storage[a]);
            if (state.storage[store[0]] >= bouquet.total) {
                bouquet.total = 0;
                state.storage[store[0]] -= bouquet.total;
            }
            else {
                bouquet.total -= state.storage[store[0]];
                state.storage[store[0]] = 0;
            }
            return updateState(state);
        }
    }
    return state;
}

exports.emptyBouquet = emptyBouquet;
exports.processInputFn = processInputFn;